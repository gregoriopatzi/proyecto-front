import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

import store from './store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
      meta: {requireAuth: true}
    },
    {
      path: '/noticias',
      name: 'noticias',
      component: () => import(/* webpackChunkName: "about" */ './views/Noticias.vue'),
      meta: {requireAuth: true}
    },
    {
      path: '/categorias',
      name: 'categorias',
      component: () => import(/* webpackChunkName: "about" */ './views/Categoria.vue')
    },
    {
      path: '/usuarios',
      name: 'usuarios',
      component: () => import(/* webpackChunkName: "about" */ './views/Users.vue'),
      meta: {requireAuth: true}
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {

  const rutaProtegida = to.matched.some(record => record.meta.requireAuth)

  if(rutaProtegida && store.state.token === ''){

    next({name: 'login'});

  }else{
    next();
  }


});


export default router;


